#%% load library
import pandas as pd
import numpy as np
import datetime
import re
import matplotlib.pyplot as plt
import datetime as dt
import warnings
import tensorflow as tf
warnings.filterwarnings('ignore')

# create list of TimeFrame
lst_airport = ['ATL', 'ORD', 'DFW', 'DEN', 'LAX', 'PHX', 'SFO', 'IAH', 'LAS', 'MSP', 'SEA', 'MCO', 'DTW', 'BOS', 'CLT', 'EWR', 'SLC', 'LGA', 'JFK', 'BWI', 'MDW', 'FLL', 'DCA', 'SAN', 'MIA', 'PHL', 'TPA', 'DAL', 'HOU']
def my_hist(df,r,name,dens):
    freq,bound = np.histogram(df,bins=r,density=dens)
    return list(zip([name+str(i) for i in range(len(freq))],freq))

#%% load Data
df = pd.read_csv('../bin/input/flights.csv',usecols=['YEAR','MONTH','DAY','FLIGHT_NUMBER','TAIL_NUMBER',
                                                        'ORIGIN_AIRPORT','DESTINATION_AIRPORT',
                                                        'SCHEDULED_DEPARTURE','DEPARTURE_TIME','WHEELS_OFF',
                                                        'DEPARTURE_DELAY','TAXI_OUT',
                                                         'AIR_TIME','DISTANCE',
                                                        'SCHEDULED_ARRIVAL','ARRIVAL_TIME','WHEELS_ON',
                                                        'ARRIVAL_DELAY','TAXI_IN',
                                                        'DIVERTED','CANCELLED'])
df = df.loc[df.DESTINATION_AIRPORT.isin(lst_airport)]
# remove cancelled and diverted
df = df.loc[(df.DIVERTED==0) & (df.CANCELLED==0)]
df = df.drop(columns=['DIVERTED','CANCELLED'])




df['plan_time_depart_hour'] = df['SCHEDULED_ARRIVAL']//100
df['plan_time_depart_minute'] = (df['SCHEDULED_ARRIVAL']%100)/60
df['plan_time_arrive_hour'] = df['SCHEDULED_DEPARTURE']//100
df['plan_time_arrive_minute'] = (df['SCHEDULED_DEPARTURE']%100)/60

df['SCHEDULED_ARRIVAL'] = df['plan_time_arrive_hour']+df['plan_time_arrive_minute']
df['SCHEDULED_DEPARTURE'] = df['plan_time_depart_hour']+df['plan_time_depart_minute']



#%%
for airport in lst_airport:
    df_in = df.loc[(df.DESTINATION_AIRPORT == airport) ]
    df_out = df.loc[(df.ORIGIN_AIRPORT == airport)]
    df_in['dt_plan'] = [datetime.datetime(y,m,d,int(M.zfill(4)[:2].replace('24','0')),int(M.zfill(4)[2:]),1) 
                for y,m,d,M in zip(df_in.YEAR.astype(int),
                                   df_in.MONTH.astype(int),
                                   df_in.DAY.astype(int),
                                   df_in.SCHEDULED_ARRIVAL.astype(int).astype(str))]
    df_out['dt_plan'] = [datetime.datetime(y,m,d,int(M.zfill(4)[:2].replace('24','0')),int(M.zfill(4)[2:]),1) 
                for y,m,d,M in zip(df_out.YEAR.astype(int),
                                   df_out.MONTH.astype(int),
                                   df_out.DAY.astype(int),
                                   df_out.SCHEDULED_DEPARTURE.astype(int).astype(str))]

    df_in['dt_actual'] = [datetime.datetime(y,m,d,int(M.zfill(4)[:2].replace('24','0')),int(M.zfill(4)[2:]),1) 
                for y,m,d,M in zip(df_in.YEAR.astype(int),
                                   df_in.MONTH.astype(int),
                                   df_in.DAY.astype(int),
                                   df_in.ARRIVAL_TIME.astype(int).astype(str))]
    df_out['dt_actual'] = [datetime.datetime(y,m,d,int(M.zfill(4)[:2].replace('24','0')),int(M.zfill(4)[2:]),1) 
                for y,m,d,M in zip(df_out.YEAR.astype(int),
                                   df_out.MONTH.astype(int),
                                   df_out.DAY.astype(int),
                                   df_out.DEPARTURE_TIME.astype(int).astype(str))]
    df_in['dt_block'] = [datetime.datetime(d.year,d.month,d.day,d.hour,d.minute-d.minute%15,1) for d in df_in.dt_actual]
    df_out['dt_block'] = [datetime.datetime(d.year,d.month,d.day,d.hour,d.minute-d.minute%15,1) for d in df_out.dt_actual]
    
    lst_block = df_in['dt_block'].unique()
    lst_features = []
    for i in range(len(lst_block)-1):
        # analyze current block
        block = lst_block[i]
        df_in_ = df_in.loc[df_in.dt_block==block]
        df_out_ = df_out.loc[df_out.dt_block==block]
        features = []
        features.append([('state_number_d',len(df_out_))])
        features.append([('state_number_a',len(df_in_))])
        
        features.append(my_hist(df_out_.DEPARTURE_DELAY,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_delay_d_',False))
        features.append(my_hist(df_out_.DEPARTURE_DELAY,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_norm_delay_d_',True))
        
        features.append(my_hist(df_out_.TAXI_OUT,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_taxi_d_',False))
        features.append(my_hist(df_out_.TAXI_OUT,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_norm_taxi_d_',True))
        
        features.append(my_hist(df_in_.ARRIVAL_DELAY,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_delay_a_',False))
        features.append(my_hist(df_in_.ARRIVAL_DELAY,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_norm_delay_a_',True))
        
        features.append(my_hist(df_in_.TAXI_IN,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_taxi_a_',False))
        features.append(my_hist(df_in_.TAXI_IN,[-np.inf,-15,-5,5,15,30,45,np.inf],'state_norm_taxi_a_',True))
        
        # analyze next block
        block = lst_block[i+1]
        df_in_ = df_in.loc[df_in.dt_block==block]
        df_out_ = df_out.loc[df_out.dt_block==block]
        features.append([('plan_number_a',len(df_in_))])
        features.append([('plan_number_d',len(df_out_))])
        
        
        features.append(my_hist(df_in_.DISTANCE,[0,500,1000,2000,4000,8000,np.inf],'plan_distance_a_',False))
        features.append(my_hist(df_in_.DISTANCE,[0,500,1000,2000,4000,8000,np.inf],'plan_norm_distance_a_',True))
        features.append(my_hist(df_in_.DISTANCE,[0,500,1000,2000,4000,8000,np.inf],'plan_distance_d_',False))
        features.append(my_hist(df_out_.DISTANCE,[0,500,1000,2000,4000,8000,np.inf],'plan_norm_distance_d_',True))
        features.append(my_hist(df_out_.ARRIVAL_DELAY,[-np.inf,-15,-5,5,15,30,45,np.inf],'plan_delay_a_',False))
  
        
        
        #merge features
        features = dict([f for ff in features for f in ff])
        lst_features.append(features)
    features = pd.DataFrame(lst_features,index=lst_block[:-1])
    features['airport'] = airport
    features.to_csv('../bin/temp/features_'+airport+'.csv')

#%%
