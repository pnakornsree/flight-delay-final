#%% load library
from copy import deepcopy
import pandas as pd
import numpy as np
import datetime
import re
import matplotlib.pyplot as plt
import datetime as dt
import warnings
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import tensorflow as tf
warnings.filterwarnings('ignore')
from sklearn import metrics,model_selection
from sklearn.ensemble import RandomForestClassifier,GradientBoostingClassifier,RandomForestRegressor
# create list of TimeFrame
lst_airport = ['ATL', 'ORD', 'DFW', 'DEN', 'LAX', 'PHX', 'SFO', 'IAH', 'LAS', 'MSP', 'SEA', 'MCO', 'DTW', 'BOS', 'CLT', 'EWR', 'SLC', 'LGA', 'JFK', 'BWI', 'MDW', 'FLL', 'DCA', 'SAN', 'MIA', 'PHL', 'TPA', 'DAL', 'HOU']
lst_airport = ['ATL', 'ORD', 'DFW', 'DEN']

#%%
#init model
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.LSTM(48, return_sequences=True))
model1.add(tf.keras.layers.LSTM(32, return_sequences=False))
model1.add(tf.keras.layers.Dense(5))
model1.add(tf.keras.layers.Activation('softmax'))
model1.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])


rf = RandomForestRegressor(n_estimators = 0  ,max_depth = 12,min_samples_split = 2   ,warm_start = True    )

#%% pre-train

for airport in lst_airport[:]:
    features_1 = pd.read_csv('../bin/temp/input_'+airport+'.csv').fillna(0)
    X1 = features_1.drop(['airport','y0','y1','y2','y3','y4'],axis=1)
    Y1 = features_1[['y0','y1','y2','y3','y4']]
    features_2 = pd.read_csv('../bin/temp/features_'+airport+'.csv').drop(['Unnamed: 0','airport'],axis=1).fillna(0)
    
    norm = features_2[['plan_delay_a_0','plan_delay_a_1','plan_delay_a_2',
                       'plan_delay_a_3','plan_delay_a_4','plan_delay_a_5','plan_delay_a_6']].sum(axis=1)
    features_2['plan_delay_a_2'] = features_2[['plan_delay_a_0','plan_delay_a_1','plan_delay_a_2']].sum(axis=1)/norm
    features_2['plan_delay_a_3']  = features_2['plan_delay_a_3'] /norm
    features_2['plan_delay_a_4']  = features_2['plan_delay_a_4'] /norm
    features_2['plan_delay_a_5']  = features_2['plan_delay_a_5'] /norm
    features_2['plan_delay_a_6']  = features_2['plan_delay_a_6'] /norm
    features_2 = features_2.loc[norm>0]
    X2 = features_2.drop(['plan_delay_a_0','plan_delay_a_1','plan_delay_a_2',
                          'plan_delay_a_3','plan_delay_a_4','plan_delay_a_5','plan_delay_a_6'],axis=1)
    Y2 = features_2[['plan_delay_a_2','plan_delay_a_3','plan_delay_a_4','plan_delay_a_5','plan_delay_a_6']]
    
    
    
    # fit model

    
    data_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(pd.concat([X1,Y1],axis=1).values,Y1.values,
                                                                 shuffle = False,length = 24,batch_size = 512)
    for ep in range(10):
        lst_y_predicted = []
        lst_y = [] 
        for mini_batch in data_gen:
            x = mini_batch[0][:,:,:-5]
            y = mini_batch[0][:,-1,-5:]
            model1.train_on_batch(x.astype('float'),y.astype('float')) 


    # fit random forest
    rf.n_estimators +=10
    rf.fit(X2.iloc[:len(X2)-len(X2)//10],Y2.iloc[:len(X2)-len(X2)//10])

#%% actual train
lst_lstm_actual = []
lst_lstm_predict = []
lst_density_actual = []
lst_density_predict = []
for airport in lst_airport[:]:
    features_1 = pd.read_csv('../bin/temp/input_'+airport+'.csv').fillna(0)
    X1 = features_1.drop(['airport','y0','y1','y2','y3','y4'],axis=1)
    Y1 = features_1[['y0','y1','y2','y3','y4']]
    features_2 = pd.read_csv('../bin/temp/features_'+airport+'.csv').drop(['Unnamed: 0','airport'],axis=1).fillna(0)
    
    norm = features_2[['plan_delay_a_0','plan_delay_a_1','plan_delay_a_2',
                       'plan_delay_a_3','plan_delay_a_4','plan_delay_a_5','plan_delay_a_6']].sum(axis=1)
    features_2['plan_delay_a_2'] = features_2[['plan_delay_a_0','plan_delay_a_1','plan_delay_a_2']].sum(axis=1)/norm
    features_2['plan_delay_a_3']  = features_2['plan_delay_a_3'] /norm
    features_2['plan_delay_a_4']  = features_2['plan_delay_a_4'] /norm
    features_2['plan_delay_a_5']  = features_2['plan_delay_a_5'] /norm
    features_2['plan_delay_a_6']  = features_2['plan_delay_a_6'] /norm
    features_2 = features_2.loc[norm>0]
    X2 = features_2.drop(['plan_delay_a_0','plan_delay_a_1','plan_delay_a_2',
                          'plan_delay_a_3','plan_delay_a_4','plan_delay_a_5','plan_delay_a_6'],axis=1)
    Y2 = features_2[['plan_delay_a_2','plan_delay_a_3','plan_delay_a_4','plan_delay_a_5','plan_delay_a_6']]
    
    
    
    # fit model rf
    rf_ = deepcopy(rf)
    rf_.n_estimators +=1000
    rf_.fit(X2.iloc[:len(X2)-len(X2)//10],Y2.iloc[:len(X2)-len(X2)//10])
    y_predicted = rf_.predict(X2.iloc[:len(X2)//10])
    y = Y2.apply(lambda x:(x==np.max(x)),axis=1).astype('float').iloc[:len(X2)//10].values
    y_reg = -5*y[:,0]+10*y[:,1]+25*y[:,2]+40*y[:,3]+60*y[:,4]
    y_reg_predict = -5*y_predicted[:,0]+10*y_predicted[:,1]+25*y_predicted[:,2]+40*y_predicted[:,3]+60*y_predicted[:,4]
    r2 = metrics.r2_score(y_reg,y_reg_predict)
    plt.title('Density based method: '+airport + ' (R2 = '+str(r2)[:5]+')')
    for i in range(5):
        fpr, tpr, _ = metrics.roc_curve(y[:,i],y_predicted[:,i] )
        auc = metrics.roc_auc_score(y[:,i],y_predicted[:,i])
        plt.plot(fpr,tpr,label=str(i)+"-auc="+str(auc))
        plt.legend(loc=4)
    plt.grid()
    plt.show()

    lst_density_actual.append(y)
    lst_density_predict.append(y_predicted)
    
    
    # fit model lstm
    model1_ = tf.keras.models.clone_model(model1)
    model1_.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
    data_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(pd.concat([X1.iloc[:len(X1)-len(X1)//10],Y1.iloc[:len(X1)-len(X1)//10]],axis=1).values,Y1.iloc[:len(X1)-len(X1)//10].values,
                                                                 shuffle = False,length = 24,batch_size = 256)
    data_gen_test = tf.keras.preprocessing.sequence.TimeseriesGenerator(pd.concat([X1.iloc[:len(X1)//10],Y1.iloc[:len(X1)//10]],axis=1).values,Y1.iloc[:len(X1)//10].values,
                                                                 shuffle = False,length = 24,batch_size = len(X1)//10)
    for ep in range(30):
        lst_y = [] 
        for mini_batch in data_gen:
            x = mini_batch[0][:,:,:-5]
            y = mini_batch[0][:,-1,-5:]
            model1_.train_on_batch(x.astype('float'),y.astype('float')) 
            

            
    y_predicted = model1_.predict_on_batch(data_gen_test[0][0][:,:,:-5].astype('float'))
    y = data_gen_test[0][0][:,-1,-5:]
    y_reg = -5*y[:,0]+10*y[:,1]+25*y[:,2]+40*y[:,3]+60*y[:,4]
    y_reg_predict = -5*y_predicted[:,0]+10*y_predicted[:,1]+25*y_predicted[:,2]+40*y_predicted[:,3]+60*y_predicted[:,4]
    r2 = metrics.r2_score(y_reg,y_reg_predict)
    # gen result 
    
    plt.title('LSTM method: '+airport + '(R2 = '+str(r2)[:5]+')')
    for i in range(5):
        fpr, tpr, _ = metrics.roc_curve(y[:,i],y_predicted[:,i] )
        auc = metrics.roc_auc_score(y[:,i],y_predicted[:,i])
        plt.plot(fpr,tpr,label=str(i)+"-auc="+str(auc))
        plt.legend(loc=4)
    plt.grid()
    plt.show()
    lst_lstm_actual.append(y)
    lst_lstm_predict.append(y_predicted)
    
#%% plot all AUC
lst_lstm_actual = np.concatenate(lst_lstm_actual)
lst_lstm_predict = np.concatenate(lst_lstm_predict)
lst_density_actual = np.concatenate(lst_density_actual)
lst_density_predict = np.concatenate(lst_density_predict)
#%%
r2 = metrics.r2_score(lst_lstm_actual,lst_lstm_predict)
plt.title('LSTM method: ALL (R2 = '+str(r2)[:5]+')')
for i in range(5):
    fpr, tpr, _ = metrics.roc_curve(lst_lstm_actual[:,i],lst_lstm_predict[:,i] )
    auc = metrics.roc_auc_score(lst_lstm_actual[:,i],lst_lstm_predict[:,i])
    plt.plot(fpr,tpr,label=str(i)+"-auc="+str(auc))
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.legend(loc=4)
plt.grid()
plt.show()
r2 = metrics.r2_score(lst_density_actual,lst_density_predict)
plt.title('Density based method: ALL (R2 = '+str(r2)[:5]+')')
for i in range(5):
    fpr, tpr, _ = metrics.roc_curve(lst_density_actual[:,i],lst_density_predict[:,i] )
    auc = metrics.roc_auc_score(lst_density_actual[:,i],lst_density_predict[:,i])
    plt.plot(fpr,tpr,label=str(i)+"-auc="+str(auc))
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.legend(loc=4)
plt.grid()
plt.show()
#%% feature importance

df_imp  = pd.DataFrame({'feature':X2.columns,'imp':rf.feature_importances_}).sort_values('imp',ascending=False)
df_imp.to_csv('../bin/output/classifier_imp.csv',index=False)

#%%

    