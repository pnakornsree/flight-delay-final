#%% load library
import pandas as pd
import numpy as np
import datetime
import re
import matplotlib.pyplot as plt
import datetime as dt
import warnings
import tensorflow as tf
warnings.filterwarnings('ignore')
pd.options.display.max_columns = 999
# create list of TimeFrame
lst_airport = ['ATL', 'ORD', 'DFW', 'DEN', 'LAX', 'PHX', 'SFO', 'IAH', 'LAS', 'MSP', 'SEA', 'MCO', 'DTW', 'BOS', 'CLT', 'EWR', 'SLC', 'LGA', 'JFK', 'BWI', 'MDW', 'FLL', 'DCA', 'SAN', 'MIA', 'PHL', 'TPA', 'DAL', 'HOU']


#%% load Data
df = pd.read_csv('../bin/input/flights.csv',usecols=['YEAR','MONTH','DAY','FLIGHT_NUMBER','TAIL_NUMBER',
                                                        'ORIGIN_AIRPORT','DESTINATION_AIRPORT',
                                                        'SCHEDULED_DEPARTURE','DEPARTURE_TIME','WHEELS_OFF',
                                                        'DEPARTURE_DELAY','TAXI_OUT',
                                                         'AIR_TIME','DISTANCE',
                                                        'SCHEDULED_ARRIVAL','ARRIVAL_TIME','WHEELS_ON',
                                                        'ARRIVAL_DELAY','TAXI_IN',
                                                        'DIVERTED','CANCELLED'])
df = df.loc[df.DESTINATION_AIRPORT.isin(lst_airport)]
# remove cancelled and diverted
df = df.loc[(df.DIVERTED==0) & (df.CANCELLED==0)]
df = df.drop(columns=['DIVERTED','CANCELLED'])
df['plan_time_depart_hour'] = df['SCHEDULED_ARRIVAL']//100
df['plan_time_depart_minute'] = (df['SCHEDULED_ARRIVAL']%100)/60
df['plan_time_arrive_hour'] = df['SCHEDULED_DEPARTURE']//100
df['plan_time_arrive_minute'] = (df['SCHEDULED_DEPARTURE']%100)/60

df['SCHEDULED_ARRIVAL'] = df['plan_time_arrive_hour']+df['plan_time_arrive_minute']
df['SCHEDULED_DEPARTURE'] = df['plan_time_depart_hour']+df['plan_time_depart_minute']


#%% features extraction
features_ref  = pd.DataFrame()
features_ref['t'] = [dt.date(y,m,d) for y,m,d in zip(df.YEAR,df.MONTH,df.DAY)]
features_ref['t1'] = [d.day for d in features_ref['t']]
features_ref['t2'] = [d.weekday() for d in features_ref['t']]
features_ref['t3'] = [d.month for d in features_ref['t']]
features_ref['t4'] = [d.month//3 for d in features_ref['t']]
features_ref['s1'] = df['ORIGIN_AIRPORT']
features_ref['s2'] = df['SCHEDULED_DEPARTURE']
features_ref['s3'] = df['DESTINATION_AIRPORT']
features_ref['s4'] = df['SCHEDULED_ARRIVAL']

features_ref  = features_ref.dropna()
features_ref = pd.get_dummies(features_ref,columns = ['s1'],prefix_sep = '_')


features_ref = features_ref.sort_values('t',ascending = True)

# defind target 
df['ARRIVAL_DELAY'] = df['ARRIVAL_DELAY'].fillna(0)
features_ref['y0'] =  (df['ARRIVAL_DELAY']<=0).astype('float') 
features_ref['y1'] = ((df['ARRIVAL_DELAY']>0) & (df['ARRIVAL_DELAY']<=15)).astype('float') 
features_ref['y2'] = ((df['ARRIVAL_DELAY']>15) & (df['ARRIVAL_DELAY']<=30)).astype('float') 
features_ref['y3'] = ((df['ARRIVAL_DELAY']>30) & (df['ARRIVAL_DELAY']<=45)).astype('float') 
features_ref['y4'] = (df['ARRIVAL_DELAY']>45).astype('float') 



#%%
for airport in lst_airport:
    features_ref_ = features_ref.loc[features_ref.s3 == airport]
    features_ref_ = features_ref_.drop(['s3','t'],axis=1)
    features_ref_['airport'] = airport
    features_ref_.to_csv('../bin/temp/input_'+airport+'.csv',index=False)

#%% init model
model = tf.keras.Sequential()
model.add(tf.keras.layers.LSTM(64, return_sequences=True))
model.add(tf.keras.layers.LSTM(32, return_sequences=True))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(5)))
model.add(tf.keras.layers.Activation('softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
#%%
for airport in lst_airport:
    features_ref_ = features_ref.loc[features_ref.s3 == airport]
    features_ref_ = features_ref_.drop(['s3','t'],axis=1)
    print(features_ref_.shape)
    
    data_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(features_ref_.values,features_ref_.values,
                                                                 shuffle = False,length = 12,batch_size = 256)
    for ep in range(100):
        lst_y_predicted = []
        lst_y = [] 
        for mini_batch in data_gen:
            x = mini_batch[0][:,:,:-5]
            y = mini_batch[0][:,:,-5:]
            model.train_on_batch(x.astype('float'),y.astype('float')) 
            y_predicted = model.predict_on_batch(x.astype('float'))
            lst_y_predicted.append(y_predicted[:,-1,:])
            lst_y.append(y[:,-1,:])
        
        y_predicted = np.concatenate(lst_y_predicted)
        y = np.concatenate(lst_y)
        y_predicted_regression = 10*y_predicted[:,1] +25*y_predicted[:,2] + 40*y_predicted[:,3]  + 60*y_predicted[:,4] 
        y_regression = 10*y[:,1] +25*y[:,2] + 40*y[:,3]  + 60*y[:,4] 
        
        plt.plot(y_regression[:600]);plt.plot(y_predicted_regression[:600])
        plt.show()
#%%

